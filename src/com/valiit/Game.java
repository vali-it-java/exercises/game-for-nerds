package com.valiit;

import java.util.Random;
import java.util.Scanner;

public class Game {

    private static final int MAX_NUMBER = 10_000;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        int randomlyGeneratedNumber = random.nextInt(MAX_NUMBER) + 1;
        int guessCounter = 0;
        int guessNumber;
        do {
            guessCounter++;
            System.out.print("Sisesta number: ");
            guessNumber = scanner.nextInt();

            if (guessNumber > randomlyGeneratedNumber) {
                System.out.println("Liiga suur");
            } else if (guessNumber < randomlyGeneratedNumber) {
                System.out.println("Liiga väike");
            }

        } while (guessNumber != randomlyGeneratedNumber);

        System.out.printf("Õige, sul kulus %d korda, et ära arvata", guessCounter);
    }
}
